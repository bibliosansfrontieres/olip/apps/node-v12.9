ARG ARCH
FROM offlineinternet/olip-base-$ARCH:latest

WORKDIR /tmp/

RUN apt update && \
    apt install python3 g++ make -y && \
    wget https://nodejs.org/dist/v12.9.0/node-v12.9.0.tar.gz && \
    tar -xzvf node-v12.9.0.tar.gz && \
    cd node-v12.9.0 && \
    ./configure && \
    make -j$(getconf _NPROCESSORS_ONLN) && \
    make install && \
    cd / && \
    rm -rf /tmp/*
